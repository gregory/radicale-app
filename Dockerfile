FROM cloudron/base:0.10.0
MAINTAINER Johannes Zellner <johannes@cloudron.io>

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && apt-get install -y python-ldap && rm -r /var/cache/apt /var/lib/apt/lists

RUN curl -L https://github.com/Kozea/Radicale/archive/1.1.2.tar.gz | tar -xz --strip-components 1 -f -
RUN python setup.py install

ADD welcome/ /app/code/welcome/
ADD config start.sh permissions.default nginx.conf /app/code/

CMD [ "/app/code/start.sh" ]
