#!/usr/bin/env node

'use strict';

var execSync = require('child_process').execSync,
    ejs = require('ejs'),
    expect = require('expect.js'),
    fs = require('fs'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    rimraf = require('rimraf'),
    superagent = require('superagent');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

var VDIRSYNCER = process.env.VDIRSYNCER || 'vdirsyncer';

console.log('===============================================');
console.log(' This test requires vdirsyncer to be installed (using ' + VDIRSYNCER + ' )');
console.log('===============================================');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    var LOCATION = 'test';
    var app;
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --new --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can get the main page', function (done) {
        superagent.get('https://' + app.fqdn).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('can sync addressbook', function () {
        rimraf.sync('/tmp/vdirsyncer');

        var statusPath = '/tmp/vdirsyncer/status/';
        var addressBookPath = '/tmp/vdirsyncer/addressbook/';
        var testContactFile = __dirname + '/test_contact.vcf';

        mkdirp.sync(statusPath);
        mkdirp.sync(addressBookPath);

        var configFile = '/tmp/vdirsyncer/config';
        var data = {
            statusPath: statusPath,
            addressBookPath: addressBookPath,
            cardDavUrl: 'https://' + app.fqdn + '/' + username + '/addressbook.vcf/',
            username: username,
            password: password
        };

        fs.writeFileSync(configFile, ejs.render(fs.readFileSync('vdirsyncer.conf.ejs', 'utf-8'), data));
        fs.writeFileSync(addressBookPath + 'test_contact.vcf', fs.readFileSync(testContactFile));

        var env = process.env;
        env.VDIRSYNCER_CONFIG = configFile;
        execSync(VDIRSYNCER + ' discover test_contacts', { env: env, stdio: 'inherit' });
        execSync(VDIRSYNCER + ' sync', { env: env, stdio: 'inherit' });

        expect(fs.readFileSync(testContactFile, 'utf-8')).to.equal(fs.readFileSync(addressBookPath + 'test_contact.vcf', 'utf-8'));
    });

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get the main page after restore', function (done) {
        superagent.get('https://' + app.fqdn).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('addressbook is still ok', function () {
        rimraf.sync('/tmp/vdirsyncer');

        var statusPath = '/tmp/vdirsyncer/status/';
        var addressBookPath = '/tmp/vdirsyncer/addressbook/';
        var testContactFile = __dirname + '/test_contact.vcf';

        mkdirp.sync(statusPath);
        mkdirp.sync(addressBookPath);

        var configFile = '/tmp/vdirsyncer/config';
        var data = {
            statusPath: statusPath,
            addressBookPath: addressBookPath,
            cardDavUrl: 'https://' + app.fqdn + '/' + username + '/addressbook.vcf/',
            username: username,
            password: password
        };

        fs.writeFileSync(configFile, ejs.render(fs.readFileSync('vdirsyncer.conf.ejs', 'utf-8'), data));

        var env = process.env;
        env.VDIRSYNCER_CONFIG = configFile;
        console.log('VDIRSYNCER_CONFIG=/tmp/vdirsyncer/config vdirsyncer sync');
        execSync(VDIRSYNCER + ' discover test_contacts', { env: env, stdio: 'inherit' });
        execSync(VDIRSYNCER + ' sync', { env: env, stdio: 'inherit' });

        var contactRemoteFileName = fs.readdirSync(addressBookPath)[0];
        expect(contactRemoteFileName).to.be.a('string');

        var testContactFileContent = fs.readFileSync(testContactFile, 'utf-8');
        var testContactRemoteFileContent = fs.readFileSync(addressBookPath + '/' + contactRemoteFileName, 'utf-8');

        // remove the dynamic radicale id
        testContactRemoteFileContent = testContactRemoteFileContent.replace('X-RADICALE-NAME:' + contactRemoteFileName + '\n', '');

        expect(testContactFileContent).to.equal(testContactRemoteFileContent);
    });

    it('can restart app', function (done) {
        execSync('cloudron restart --app ' + app.id);
        done();
    });

    it('can get the main page after restart', function (done) {
        superagent.get('https://' + app.fqdn).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
